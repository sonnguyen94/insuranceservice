package com.ltg.insuranceservice.repository

import com.ltg.insuranceservice.entity.InsurancePolicy
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface InsurancePolicyRepository: JpaRepository<InsurancePolicy, Int>{
}