package com.ltg.insuranceservice.repository

import com.ltg.insuranceservice.entity.Insured
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface InsuredRepository: JpaRepository<Insured, Int> {

    @Query("SELECT i FROM Insured i WHERE i.policyId = ?1 and i.insurerId = ?2")
    fun find(policyId: Int, insurerId: Int): Insured?

    @Query("SELECT i FROM Insured i WHERE i.policyId = ?1")
    fun findByPolicy(policyId: Int): List<Insured>

    @Query("SELECT i FROM Insured i WHERE i.insurerId = ?1")
    fun findByInsured(insurerId: Int): List<Insured>
}