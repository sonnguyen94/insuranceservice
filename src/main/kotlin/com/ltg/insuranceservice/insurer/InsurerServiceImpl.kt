package com.ltg.insuranceservice.insurer

import com.ltg.insuranceservice.converter.InsurerConverter
import com.ltg.insuranceservice.entity.Insurer
import com.ltg.insuranceservice.repository.InsurerRepository
import com.ltg.insuranceservice.vo.InsurerResultObject
import com.ltg.insuranceservice.vo.InsurerVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList

@Service
class InsurerServiceImpl: InsurerService {

    @Autowired
    private lateinit var repository: InsurerRepository

    override fun createInsurer(insurerVO: InsurerVO?): InsurerResultObject {
        if (insurerVO === null) {
            return InsurerResultObject(null, InsurerResultObject.StatusCode.INVALID_PARAM)
        }

        val insurer = InsurerConverter.convertToEntity(insurerVO)
        repository.save(insurer!!)

        insurerVO.id = insurer.id
        return InsurerResultObject(ArrayList(listOf(insurerVO)), InsurerResultObject.StatusCode.SUCCESS)
    }

    override fun getALl(): InsurerResultObject {
        val insurers = repository.findAll()

        val insurerVOs = ArrayList<InsurerVO>()
        for (i in insurers) {
            insurerVOs.add(InsurerConverter.convertToVO(i)!!)
        }

        return InsurerResultObject(insurerVOs, InsurerResultObject.StatusCode.SUCCESS)
    }

    override fun findById(id: Int?): InsurerResultObject {
        if (id == null) {
            return InsurerResultObject(null, InsurerResultObject.StatusCode.INVALID_PARAM)
        }

        val insurerOpt: Optional<Insurer> = repository.findById(id)
        if (insurerOpt.isEmpty) {
            return InsurerResultObject(null, InsurerResultObject.StatusCode.INSURER_NOT_FOUND)
        }

        val insurerVO = InsurerConverter.convertToVO(insurerOpt.get())
        return InsurerResultObject(ArrayList(listOf(insurerVO!!)), InsurerResultObject.StatusCode.SUCCESS)
    }

    override fun updateInsurer(insurerVO: InsurerVO?): InsurerResultObject {
        if (insurerVO == null) {
            return InsurerResultObject(null, InsurerResultObject.StatusCode.INVALID_PARAM)
        }

        if (insurerVO.id == null) {
            return InsurerResultObject(null, InsurerResultObject.StatusCode.INVALID_PARAM)
        }

        val insurerOpt: Optional<Insurer> = repository.findById(insurerVO.id!!)
        if (insurerOpt.isEmpty) {
            return InsurerResultObject(null, InsurerResultObject.StatusCode.INSURER_NOT_FOUND)
        }

        val insurer = insurerOpt.get()
        insurer.fname = insurerVO.fname
        insurer.lname = insurerVO.lname
        insurer.gender = insurerVO.gender
        insurer.address = insurerVO.address
        insurer.age = insurerVO.age

        repository.save(insurer)
        return InsurerResultObject(ArrayList(listOf(insurerVO)), InsurerResultObject.StatusCode.SUCCESS)
    }

    override fun delete(id: Int?): InsurerResultObject {
        if (id == null) {
            return InsurerResultObject(null, InsurerResultObject.StatusCode.INVALID_PARAM)
        }

        val policyOpt: Optional<Insurer> = repository.findById(id)
        if (policyOpt.isEmpty) {
            return InsurerResultObject(null, InsurerResultObject.StatusCode.INSURER_NOT_FOUND)
        }

        repository.delete(policyOpt.get())
        return InsurerResultObject(null, InsurerResultObject.StatusCode.SUCCESS)
    }

}