package com.ltg.insuranceservice.insurer

import com.ltg.insuranceservice.vo.InsurancePolicyResultObject
import com.ltg.insuranceservice.vo.InsurancePolicyVO
import com.ltg.insuranceservice.vo.InsurerResultObject
import com.ltg.insuranceservice.vo.InsurerVO

interface InsurerService {

    /**
     * Create new insurance policy
     */
    fun createInsurer(insurerVO: InsurerVO?): InsurerResultObject

    /**
     * get all insurance policy
     */
    fun getALl(): InsurerResultObject

    /**
     * Find the insurance policy by id
     */
    fun findById(id: Int?): InsurerResultObject

    /**
     * Update the insurance policy
     */
    fun updateInsurer(insurerVO: InsurerVO?): InsurerResultObject

    /**
     * Delete the insurance policy
     */
    fun delete(id: Int?): InsurerResultObject
}