package com.ltg.insuranceservice.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter
import java.util.*

@Configuration
@EnableWebSecurity
class SecurityConfig : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity?) {
        http?.exceptionHandling()
            ?.and()
            ?.csrf()?.disable()
            ?.headers()?.frameOptions()?.disable()
            ?.and()
            ?.cors()
            ?.and()
            ?.authorizeRequests()?.antMatchers("/**")?.permitAll()
    }

    @Bean
    fun corsFilter(): CorsFilter {
        val source = UrlBasedCorsConfigurationSource()
        val config = CorsConfiguration()

        config.allowCredentials = true;
        config.allowedOrigins = Collections.singletonList("*");
        config.allowedHeaders = Collections.singletonList("*");
        config.allowedMethods = listOf("GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH");
        source.registerCorsConfiguration("/**", config);

        return CorsFilter(source)
    }
}