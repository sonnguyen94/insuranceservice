package com.ltg.insuranceservice.domaintype

enum class GenderType() {
    MALE,
    FEMALE,
    OTHER
}