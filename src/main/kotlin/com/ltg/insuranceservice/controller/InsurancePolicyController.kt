package com.ltg.insuranceservice.controller

import com.ltg.insuranceservice.insurancepolicy.InsurancePolicyService
import com.ltg.insuranceservice.insured.InsuredService
import com.ltg.insuranceservice.vo.InsurancePolicyResultObject
import com.ltg.insuranceservice.vo.InsurancePolicyVO
import com.ltg.insuranceservice.vo.InsuredResultObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/policy")
class InsurancePolicyController {

    @Autowired
    private lateinit var policyService: InsurancePolicyService

    @Autowired
    private lateinit var insuredService: InsuredService

    @GetMapping
    fun getAll(): ResponseEntity<InsurancePolicyResultObject> {
        var ret: InsurancePolicyResultObject = try {
            policyService.getALl()
        } catch (e: Exception) {
            InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.ERROR)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Int): ResponseEntity<InsurancePolicyResultObject> {
        var ret: InsurancePolicyResultObject = try {
            policyService.findById(id)
        } catch (e: Exception) {
            InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.ERROR)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }

    @PostMapping
    fun save(@RequestBody policyVO: InsurancePolicyVO): ResponseEntity<InsurancePolicyResultObject> {
        var ret: InsurancePolicyResultObject = try {
            policyService.createPolicy(policyVO)
        } catch (e: Exception) {
            InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.ERROR)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }

    @PutMapping
    fun update(@RequestBody policyVO: InsurancePolicyVO): ResponseEntity<InsurancePolicyResultObject> {
        var ret: InsurancePolicyResultObject
        try {
            ret = policyService.updatePolicy(policyVO)
        } catch (e: Exception) {
            ret = InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.ERROR)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Int): ResponseEntity<InsurancePolicyResultObject> {
        var ret: InsurancePolicyResultObject
        try {
            ret = policyService.delete(id)
        } catch (e: Exception) {
            ret = InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.ERROR)
        }

        if (ret.isSuccessful()) {
            return ResponseEntity(null, ret.statusCode.httpStatus)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }

    @GetMapping("/{id}/insured")
    fun getAllInsured(@PathVariable id: Int): ResponseEntity<InsuredResultObject> {
        var ret: InsuredResultObject
        try {
            ret = insuredService.findByPolicy(id)
        } catch (e: Exception) {
            ret = InsuredResultObject(null, InsuredResultObject.StatusCode.ERROR)
        }

        if (ret.isSuccessful()) {
            return ResponseEntity(null, ret.statusCode.httpStatus)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }
}