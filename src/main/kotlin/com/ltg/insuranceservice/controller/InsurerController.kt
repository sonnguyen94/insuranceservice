package com.ltg.insuranceservice.controller

import com.ltg.insuranceservice.insured.InsuredService
import com.ltg.insuranceservice.insurer.InsurerService
import com.ltg.insuranceservice.vo.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/insurer")
class InsurerController {
    @Autowired
    private lateinit var insurerService: InsurerService

    @Autowired
    private lateinit var insuredService: InsuredService

    @GetMapping
    fun getAll(): ResponseEntity<InsurerResultObject> {
        var ret: InsurerResultObject = try {
            insurerService.getALl()
        } catch (e: Exception) {
            InsurerResultObject(null, InsurerResultObject.StatusCode.ERROR)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Int): ResponseEntity<InsurerResultObject> {
        var ret: InsurerResultObject = try {
            insurerService.findById(id)
        } catch (e: Exception) {
            InsurerResultObject(null, InsurerResultObject.StatusCode.ERROR)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }

    @PostMapping
    fun save(@RequestBody insurerVO: InsurerVO): ResponseEntity<InsurerResultObject> {
        var ret: InsurerResultObject = try {
            insurerService.createInsurer(insurerVO)
        } catch (e: Exception) {
            InsurerResultObject(null, InsurerResultObject.StatusCode.ERROR)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }

    @PutMapping
    fun update(@RequestBody insurerVO: InsurerVO): ResponseEntity<InsurerResultObject> {
        var ret: InsurerResultObject
        try {
            ret = insurerService.updateInsurer(insurerVO)
        } catch (e: Exception) {
            ret = InsurerResultObject(null, InsurerResultObject.StatusCode.ERROR)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Int): ResponseEntity<InsurerResultObject> {
        var ret: InsurerResultObject
        try {
            ret = insurerService.delete(id)
        } catch (e: Exception) {
            ret = InsurerResultObject(null, InsurerResultObject.StatusCode.ERROR)
        }

        if (ret.isSuccessful()) {
            return ResponseEntity(null, ret.statusCode.httpStatus)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }

    @PostMapping("/{insurerId}/{policyId}")
    fun attachToPolicy(@PathVariable insurerId: Int, @PathVariable policyId: Int): ResponseEntity<InsuredResultObject> {
        var ret: InsuredResultObject
        try {
            ret = insuredService.attach(policyId, insurerId)
        } catch (e: Exception) {
            ret = InsuredResultObject(null, InsuredResultObject.StatusCode.ERROR)
        }

        if (ret.isSuccessful()) {
            return ResponseEntity(null, ret.statusCode.httpStatus)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }

    @PutMapping("/{insurerId}/{policyId}")
    fun detachFromPolicy(@PathVariable insurerId: Int, @PathVariable policyId: Int): ResponseEntity<InsuredResultObject> {
        var ret: InsuredResultObject
        try {
            ret = insuredService.detach(policyId, insurerId)
        } catch (e: Exception) {
            ret = InsuredResultObject(null, InsuredResultObject.StatusCode.ERROR)
        }

        if (ret.isSuccessful()) {
            return ResponseEntity(ret, ret.statusCode.httpStatus)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }

    @GetMapping("/{id}/insured")
    fun getAllInsured(@PathVariable id: Int): ResponseEntity<InsuredResultObject> {
        var ret: InsuredResultObject
        try {
            ret = insuredService.findByInsurer(id)
        } catch (e: Exception) {
            ret = InsuredResultObject(null, InsuredResultObject.StatusCode.ERROR)
        }

        if (ret.isSuccessful()) {
            return ResponseEntity(ret, ret.statusCode.httpStatus)
        }

        return ResponseEntity(ret, ret.statusCode.httpStatus)
    }
}