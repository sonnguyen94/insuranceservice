package com.ltg.insuranceservice.insured

import com.ltg.insuranceservice.vo.InsuredResultObject

interface InsuredService {

    /**
     * Attach an insurer to a insurance policy
     */
    fun attach(policyId: Int?, insurerId: Int?): InsuredResultObject

    /**
     * Detach an insurer from a insurance policy
     */
    fun detach(policyId: Int?, insurerId: Int?): InsuredResultObject

    /**
     * Find by policy
     */
    fun findByPolicy(policyId: Int?): InsuredResultObject

    /**
     * find by insurer
     */
    fun findByInsurer(insurerId: Int?): InsuredResultObject
}