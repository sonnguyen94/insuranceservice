package com.ltg.insuranceservice.insurancepolicy

import com.ltg.insuranceservice.converter.InsurancePolicyConverter
import com.ltg.insuranceservice.entity.InsurancePolicy
import com.ltg.insuranceservice.repository.InsurancePolicyRepository
import com.ltg.insuranceservice.vo.InsurancePolicyResultObject
import com.ltg.insuranceservice.vo.InsurancePolicyVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList

@Service
class InsurancePolicyServiceImpl : InsurancePolicyService {

    @Autowired
    private lateinit var repository: InsurancePolicyRepository

    override fun createPolicy(policyVO: InsurancePolicyVO?): InsurancePolicyResultObject {
        if (policyVO == null) {
            return InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.INVALID_PARAM)
        }

        if (policyVO.name == null || policyVO.name!!.trim().isEmpty()) {
            return InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.INVALID_PARAM)
        }

        var policy = InsurancePolicyConverter.convertToEntity(policyVO)
        policy = repository.save(policy!!)

        policyVO.id = policy.id
        return InsurancePolicyResultObject(ArrayList(listOf(policyVO)), InsurancePolicyResultObject.StatusCode.SUCCESS)
    }

    override fun getALl(): InsurancePolicyResultObject {
        val policies = repository.findAll()

        if (policies.isEmpty()) {
            return  InsurancePolicyResultObject(ArrayList(), InsurancePolicyResultObject.StatusCode.SUCCESS)
        }

        val policyVOs = ArrayList<InsurancePolicyVO>()
        for (p in policies) {
            policyVOs.add(InsurancePolicyConverter.convertToVO(p)!!)
        }

        return InsurancePolicyResultObject(policyVOs, InsurancePolicyResultObject.StatusCode.SUCCESS)
    }

    override fun findById(id: Int?): InsurancePolicyResultObject {
        if (id == null) {
            return InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.INVALID_PARAM)
        }

        val policy: Optional<InsurancePolicy> = repository.findById(id)
        if (policy.isEmpty) {
            return InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.POLICY_NOT_FOUND)
        }

        val policyVO = InsurancePolicyConverter.convertToVO(policy.get())
        return InsurancePolicyResultObject(ArrayList(listOf(policyVO!!)), InsurancePolicyResultObject.StatusCode.SUCCESS)
    }

    override fun updatePolicy(policyVO: InsurancePolicyVO?): InsurancePolicyResultObject {
        if (policyVO == null) {
            return InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.INVALID_PARAM)
        }

        if (policyVO.id == null) {
            return InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.POLICY_NOT_FOUND)
        }

        val policyOpt: Optional<InsurancePolicy> = repository.findById(policyVO.id!!)
        if (policyOpt.isEmpty) {
            return InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.POLICY_NOT_FOUND)
        }

        val policy = policyOpt.get()
        policy.name = policyVO.name
        policy.description = policyVO.description
        repository.save(policy)

        return InsurancePolicyResultObject(ArrayList(listOf(policyVO)), InsurancePolicyResultObject.StatusCode.SUCCESS)
    }

    override fun delete(id: Int?): InsurancePolicyResultObject {
        if (id == null) {
            return InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.INVALID_PARAM)
        }

        val policyOpt: Optional<InsurancePolicy> = repository.findById(id)
        if (policyOpt.isEmpty) {
            return InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.POLICY_NOT_FOUND)
        }

        repository.delete(policyOpt.get())
        return InsurancePolicyResultObject(null, InsurancePolicyResultObject.StatusCode.SUCCESS)
    }
}