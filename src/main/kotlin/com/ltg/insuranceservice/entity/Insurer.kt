package com.ltg.insuranceservice.entity

import com.ltg.insuranceservice.domaintype.GenderType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity(name="Insurer")
data class Insurer(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int?
) {

    var fname: String? = null

    var lname: String? = null

    var gender: GenderType? = null

    var address: String? = null

    var age: Int? = null
}