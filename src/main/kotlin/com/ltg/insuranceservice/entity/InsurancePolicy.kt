package com.ltg.insuranceservice.entity

import javax.persistence.*

@Entity(name="InsurancePolicy")
data class InsurancePolicy(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int?,

    @Column(nullable = false)
    var name: String?
) {

    @Column(nullable = true)
    var description: String? = null
}