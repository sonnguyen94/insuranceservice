package com.ltg.insuranceservice.entity

import javax.persistence.*

@Entity(name = "Insured")
data class Insured(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int?,

    @Column(name = "start_date", nullable = false)
    val startDate: String?,

    @Column(name = "status", nullable = false)
    val status: String?,
) {
    var policyId: Int? = null

    var insurerId: Int? = null

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    var insurancePolicy: InsurancePolicy? = null

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    var insurer: Insurer? = null
}