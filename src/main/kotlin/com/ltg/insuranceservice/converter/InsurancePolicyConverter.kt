package com.ltg.insuranceservice.converter

import com.ltg.insuranceservice.entity.InsurancePolicy
import com.ltg.insuranceservice.vo.InsurancePolicyVO

class InsurancePolicyConverter {
    companion object {
        fun convertToVO(entity: InsurancePolicy?): InsurancePolicyVO? {
            if (entity == null) {
                return null
            }

            val ret = InsurancePolicyVO()
            ret.id = entity.id
            ret.name = entity.name
            ret.description = entity.description

            return ret;
        }

        fun convertToEntity(vo: InsurancePolicyVO?): InsurancePolicy? {
            if (vo == null) {
                return null
            }

            val ret = InsurancePolicy(vo.id, vo.name)
            ret.description = vo.description

            return ret
        }
    }
}