package com.ltg.insuranceservice.converter

import com.ltg.insuranceservice.entity.Insured
import com.ltg.insuranceservice.vo.InsuredVO

class InsuredConverter {
    companion object {
        fun convertToVO(entity: Insured?): InsuredVO? {
            if (entity == null) {
                return null
            }

            val ret = InsuredVO()
            ret.id = entity.id
            ret.startDate = entity.startDate
            ret.status = entity.status
            ret.insurancePolicy = InsurancePolicyConverter.convertToVO(entity.insurancePolicy)!!
            ret.insurer = InsurerConverter.convertToVO(entity.insurer)!!

            return ret;
        }

        fun convertToEntity(vo: InsuredVO?): Insured? {
            if (vo == null) {
                return null
            }

            val ret = Insured(
                vo.id,
                vo.startDate,
                vo.status
            )

            return ret
        }
    }
}