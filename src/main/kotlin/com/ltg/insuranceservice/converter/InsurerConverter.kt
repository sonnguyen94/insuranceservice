package com.ltg.insuranceservice.converter

import com.ltg.insuranceservice.entity.InsurancePolicy
import com.ltg.insuranceservice.entity.Insurer
import com.ltg.insuranceservice.vo.InsurancePolicyVO
import com.ltg.insuranceservice.vo.InsurerVO

class InsurerConverter {
    companion object {
        fun convertToVO(entity: Insurer?): InsurerVO? {
            if (entity == null) {
                return null
            }

            val ret = InsurerVO()
            ret.id = entity.id
            ret.fname = entity.fname
            ret.lname = entity.lname
            ret.gender = entity.gender
            ret.address = entity.address
            ret.age = entity.age

            return ret;
        }

        fun convertToEntity(entity: InsurerVO?): Insurer? {
            if (entity == null) {
                return null
            }

            val ret = Insurer(entity.id)
            ret.fname = entity.fname
            ret.lname = entity.lname
            ret.gender = entity.gender
            ret.address = entity.address
            ret.age = entity.age

            return ret
        }
    }
}