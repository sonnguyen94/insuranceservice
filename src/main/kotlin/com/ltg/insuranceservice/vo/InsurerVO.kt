package com.ltg.insuranceservice.vo

import com.ltg.insuranceservice.domaintype.GenderType
import java.io.Serializable

class InsurerVO: Serializable {

    var id: Int? = null

    var fname: String? = null

    var lname: String? = null

    var gender: GenderType? = null

    var address: String? = null

    var age: Int? = null
}