package com.ltg.insuranceservice.vo

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonValue
import lombok.AllArgsConstructor
import lombok.Getter
import org.springframework.http.HttpStatus
import java.io.Serializable

@JsonInclude
class InsuredResultObject(

    @JsonProperty("data")
    val data: ArrayList<InsuredVO>?,

    @JsonProperty("status_code")
    val statusCode: StatusCode
) : Serializable {

    @Getter
    @AllArgsConstructor
    enum class StatusCode(
        @JsonValue
        private val message: String?,
        val httpStatus: HttpStatus
    ) {
        SUCCESS("Success", HttpStatus.OK),
        CREATED("Created", HttpStatus.CREATED),
        NO_CONTENT("No content", HttpStatus.NO_CONTENT),
        ERROR("Error", HttpStatus.INTERNAL_SERVER_ERROR),

        POLICY_NOT_FOUND("No insurance policy was found", HttpStatus.BAD_REQUEST),
        INSURER_NOT_FOUND("No insurer was found", HttpStatus.BAD_REQUEST),
        INSURED_NOT_FOUND("No data was found", HttpStatus.BAD_REQUEST),
        INVALID_PARAM("The input parameter is invalid", HttpStatus.BAD_REQUEST);
    }

    /**
     * Test if current result object contain a "successful" status flag.
     *
     * @return if current result is considered a success or not, true if success!
     */
    @JsonIgnore
    fun isSuccessful(): Boolean {
        return getSuccessfulStatusValue() === statusCode
    }

    @JsonIgnore
    fun getErrorStatusValue(): StatusCode {
        return StatusCode.ERROR
    }

    @JsonIgnore
    fun getSuccessfulStatusValue(): StatusCode {
        return StatusCode.SUCCESS
    }
}