package com.ltg.insuranceservice.util

import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*

class DateTimeUtil {
    companion object {
        private const val DATETIME_FORMAT = "MM-dd-yyyy hh:mm:ss"

        fun getDateTime(milli: Long): String {
            val date = Date(milli)
            var formatter = SimpleDateFormat(DATETIME_FORMAT)

            return formatter.format(date)
        }
    }
}